import './style/index.scss';
import {modalWindowListener} from './module/modalWindow';
import {HomePage} from '@/views/HomePage';
import {router} from './router/router';


let template = require('./partial/template.handlebars')

document.addEventListener('DOMContentLoaded', () => {
  document.body.innerHTML = template({});

  modalWindowListener();
  if (localStorage.getItem('location')) {
    router(null, null, localStorage.getItem('location'))
  } else {
    new HomePage().init();
  }


  document.querySelector('body').addEventListener('click', event => {
    event.preventDefault();
    if (event.target.hasAttribute('data-link')) {
      router(event.target);
    }
  })
})
