const templateHome = require('../partial/Home.handlebars')

export class HomePage {

  init() {
    document.querySelector('main').innerHTML = templateHome({})
  }
}