export class GameListItem {
  rendered = false


  constructor(game) {
    this.id = game.id
    this.title = game.title
    this.thumbnail = game.thumbnail
    this.short_description = game.short_description
    this.game_url = game.game_url
    this.genre = game.genre
    this.platform = game.platform
    this.publisher = game.publisher
    this.developer = game.developer
    this.release_date = game.release_date
    this.profile_url = game.profile_url
  }

  render() {
    this.rendered = true
    return `
     <div class="gameList--item" id="${this.id}">
        <div class="gameList--title">
            <h3>${this.title}</h3>
       </div>
       <div class="gameList--img">
          <img  src="${this.thumbnail}" alt="${this.title}" loading="lazy">
       </div>
       
       <div class="gameList--more">
       <span>${this.genre} | ${this.platform}</span>
         
       </div>
      
    </div> 
    `
  }
}
