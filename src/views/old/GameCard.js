export class GameCard {
  game = {}
  gameList = document.querySelector('.game-list')
  pagination = document.querySelector('.pagination')
  container = document.querySelector('main')
  sort = document.querySelector('.sort')

  constructor(url, options) {
    this._getData(url, options)
        .then(() => this._toggleVisibility())
        .then(() => this._render())
        .then(() => this._addListener())

  }

  async _getData(url, options) {
    return await fetch(url, options)
        .then(response => response.json())
        .then(i => Object.assign(this.game, i))
  }

  _toggleVisibility() {
    this.gameList.classList.toggle('--visibility')
    this.pagination.classList.toggle('--visibility')
    this.sort.classList.toggle('--visibility')
  }

  _addListener() {
    const btn = document.querySelector('.gameCard--backToList')
    const card = document.querySelector('.gameCard')
    const listener = () => {
      this._toggleVisibility()
      card.remove()
      btn.removeEventListener('click', listener)
    }
    btn.addEventListener('click', listener)
  }

  _template() {
    console.log(this.game)
    return `
    <div class="gameCard container" id="${this.game.id}">
    <button class="gameCard--backToList">Вернутья к списку</button>
      <div class="gameCard--main">
        <div class="gameCard--img"><img src="${this.game.thumbnail}" alt="${this.game.title}"></div>
        <div class="gameCard--info">
         <p>Системные требования:</p>
          <div class="gameCard--more">
            <div class="gameCard--el"><p>Жанр: </p>  <span> ${this.game.genre}</span></div>
            <div class="gameCard--el"><p>Платформа: </p><span> ${this.game.platform}</span></div>
            <div class="gameCard--el"><p>Разработчик: </p><span> ${this.game.developer}</span></div>
            <div class="gameCard--el"><p>Дата релиза: </p><span> ${this.game.release_date}</span></div>        
            <div class="gameCard--el"><p>OC: </p><span>${this.game.minimum_system_requirements?.os ? this.game.minimum_system_requirements.os : 'Windows All'}</span></div>
            <div class="gameCard--el"><p>Процессор: </p><span>${this.game.minimum_system_requirements?.processor ? this.game.minimum_system_requirements?.processor : 'Intel or AMD'}</span></div>
            <div class="gameCard--el"><p>Видеокарта: </p><span>${this.game.minimum_system_requirements?.graphics ? this.game.minimum_system_requirements?.graphics : 'Nvidia'}</span></div>
            <div class="gameCard--el"><p>Оператива: </p><span>${this.game.minimum_system_requirements?.memory ? this.game.minimum_system_requirements?.memory : '1GB'}</span></div>
            <div class="gameCard--el"><p>Место на диске: </p><span>${this.game.minimum_system_requirements?.storage ? this.game.minimum_system_requirements?.storage : '10GB'}</span></div>
          </div>
        </div>
       </div> 
      <div class="gameCard--description">
        <div><h3>${this.game.title}</h3></div>
        <div><p>${this.game.description}</p></div>
        <div class="gameCard--wrapper">
        <p>Скриншоты: </p>
          <div class="gameCard--screenshots">
            <div><img src="${this.game.screenshots[0]?.image}" alt="#"></div>
            <div><img src="${this.game.screenshots[1]?.image}" alt="#"></div>
            <div><img src="${this.game.screenshots[2]?.image}" alt="#"></div>
          </div>
        </div>  
      </div>
     </div>
    `
  }
  _render() {
    this.container.insertAdjacentHTML('beforeend', this._template())
  }
}
