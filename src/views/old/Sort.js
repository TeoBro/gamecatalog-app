import {getRequest} from "../../utility/GetRequest";
import {GameList} from "./GameList";

export class Sort {
  genres = new Set()
  platforms = new Set()
  releaseDate = new Set()
  selectedTag = []
  selectedPlatform = null
  selectedSortBy = null
  sortContainer = document.querySelector('.sort')
  btnShow = document.querySelector('.sort--btn')

  constructor(games) {
    this._gamesCategories(games)
    this._render()
    this._addListener()
  }


  _gamesCategories(games) {
    games.forEach(game => {
      if (game.platform.trim().split(',').length === 1) {
        this.platforms.add(game.platform.trim().split(',')[0])
      }
      if (Number(game.release_date.slice(0, 4) > 0)) {
        this.releaseDate.add(Number(game.release_date.slice(0, 4)))
      }
      this.genres.add(game.genre.trim())
    })
  }

  _animationSort() {
    this.sortContainer.classList.toggle('--hide')
  }

  _addListener() {
    this.sortContainer.addEventListener('click', () => {
      this._animationSort()
    })

    const sortEl = document.querySelectorAll('.sort--el')

    sortEl.forEach( el => {
      el.addEventListener('click', event => {
        event.stopPropagation()
        event.target.classList.toggle('--selected')
      })
    })

    this.btnShow.addEventListener('click', () => {

      this.selectedTag = []
      this.selectedPlatform = null
      this.selectedSortBy = null

      document.querySelectorAll('.--selected').forEach( el => {
        if(el.classList.contains('genre')) {
          this.selectedTag.push(el.id.toLowerCase().trim())
        } else if(el.classList.contains('platform')) {
          this.selectedPlatform = el.id
        } else if (el.classList.contains('sortBy')) {
          this.selectedSortBy = el.id
        }
      })
     let request = getRequest(this.selectedTag, this.selectedPlatform, this.selectedSortBy)
      document.querySelector('.game-list').classList.add('--visibility')
     new GameList( '.gameListSort', request.requestUrl, request.options)
    })
  }

  _createTemplate(type, arr) {
    let sortType
    switch(type) {
      case 'genre':
        sortType = 'Жанр'
        break
      case 'platform':
        sortType = 'Платформа'
        break
      case 'sortBy':
        sortType = 'Сортировать по'
        break
    }
    const wrapper = document.createElement('div')
    wrapper.classList.add('sort--wrapper')
    const container = document.createElement('div')
    container.classList.add('sort--container')
    const title = `<p class="sort--title">${sortType}</p>`
    let temp = ''
    arr.forEach( el => {
      temp += `<div class="sort--el ${type}" id="${el}">${el}</div>`
    })
    container.innerHTML = temp
    wrapper.insertAdjacentHTML('afterbegin', title)
    wrapper.append(container)
    return wrapper
  }

  _render() {
    this.sortContainer.append( this._createTemplate('genre', this.genres))
    this.sortContainer.append( this._createTemplate('platform', this.platforms),)
    //this.sortContainer.append( this._createTemplate('Год релиза', this.releaseDate))
  }
}
