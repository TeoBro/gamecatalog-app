export class Pagination {
  pages = 0
  container = document.querySelector('.pagination--number')

  constructor(pages) {
    this.pages = pages
    this._render()
  }

  _template(num) {
    return `
 
    `
  }

  _render() {
    let paginationHTML = ''
    for (let i = 1; i <= this.pages; i++) {
      paginationHTML += this._template(i)
    }
    return this.container.insertAdjacentHTML('beforeend', paginationHTML)
  }
}