import {GameListItem} from '@/views/GameListItem'
import {Pagination} from '@/views/old/Pagination'
import {Sort} from '@/views/old/Sort'
import {GameCard} from '@/views/old/GameCard'

export class GameList {
  games = []
  container = null
  paginationContainer = document.querySelector('.pagination--number')
  pagination = document.querySelector('.pagination')
  sort = document.querySelector('.sort')
  options = {
    method: 'GET', headers: {
      'X-RapidAPI-Key': process.env.ACCESS_TOKEN,
      'X-RapidAPI-Host': 'mmo-games.p.rapidapi.com'
    }
  }

  startTransform = 200
  pages = []
  gamePage = []
  currentPage = 0


  constructor(selector, url, options) {
    console.log(selector)
    this.container = document.querySelector(selector)
    this._init(url, options)
  }

  _getData(url, options) {
    return fetch(url, options)
        .then(response => response.json())
        .then(json => json.forEach(i => this.games.push(new GameListItem(i))))
        .then(()=> this._addLoader())
  }

  _addLoader() {
    const loader = document.querySelector('#loader')
    loader.classList.add('--visibility')
  }

  _openGameCard() {
    const el = document.querySelectorAll('.gameList--item')
    el.forEach(el => {
      el.addEventListener('click', () => {
        const url2 = `https://mmo-games.p.rapidapi.com/game?id=${el.id}`
        this.sort.classList.add('--hide')
        new GameCard(url2, this.options)
      })
    })
  }

  _paginationListener() {
    this.pagination.addEventListener('click', (event) => {
      this.changePage(event.target)
    })
  }


  _getPages() {
    let count = 15
    let pageNumber = 0
    for (let i = 0; i < this.games.length; i++) {
      if (i < count) {
        this.gamePage.push(this.games[i])
      } else {
        this.pages[pageNumber] = this.gamePage.slice(0)
        this.gamePage = []
        this.gamePage.push(this.games[i])
        count += 15
        pageNumber++
      }
    }
    this._pagesRender()
  }

  _animationPagination() {
    // eslint-disable-next-line max-len
    this.paginationContainer.style.transform = 'translateX(' + (this.startTransform - this.currentPage * 100) + 'px)'
  }

  changePage(el) {
    this.sort.classList.add('--hide')
    this.container.innerHTML = ''
    const paginationPage = document.querySelectorAll('.pagination--page')

    paginationPage.forEach(el => {
      el.classList.remove('active')
    })

    if (el.classList.contains('--next') && this.currentPage < this.pages.length - 1) {
      this.currentPage++
      this._animationPagination()
      this._pagesRender()
    } else if (el.classList.contains('--prev') && this.currentPage > 0) {
      this.currentPage--
      window.scrollTo(0,0)
      this._animationPagination()
      this._pagesRender()
    } else if (el.classList.contains('pagination--page')) {
      this.currentPage = el.innerText - 1
      window.scrollTo(0,0)
      this._animationPagination()
      this._pagesRender()
    } else {
      window.scrollTo(0,0)
      this._pagesRender()
    }
    paginationPage.forEach(el => {
      if (Number(el.innerHTML) === this.currentPage + 1) {
        window.scrollTo(0,0)
        el.classList.add('active')
      }
    })
    this._openGameCard()
  }

  _pagesRender() {
    for (let i = 0; i < this.pages[this.currentPage].length; i++) {
      this._render(this.pages[this.currentPage][i])
    }
  }

  _render(game) {

    this.container.insertAdjacentHTML('beforeend', game.render())
  }

  _init(url, options) {
    this._getData(url, options).then(() => this._getPages())
        .then(() => this._openGameCard())
      .then(() => this._paginationListener())
        .then(()=> new Pagination(this.pages.length))
        .then(() => new Sort(this.games))
  }
}
