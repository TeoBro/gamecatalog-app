import {getRequest} from '@/utility/GetRequest';
import {getElement} from '@/utility/GetElement';
import {router} from '../router/router';
import {CutHTMLTagsFromText} from '@/utility/CutHTMLTagsFromText';
import {TranslateToCenter} from '@/utility/TranslateToCenter';
import {saveToLocalStorage} from "@/utility/SaveToLocalStorage";

const gameListTemplate = require('../partial/GameCatalog.handlebars')
const paginationTemplate = require('../partial/components/pagination.handlebars')
const cardTemplate = require('../partial/components/card.handlebars')

export class GameListApp {
  data = {
    gamesList: [],
    pages: [
      {
        id: '',
        games: []
      }
    ],
    currentPage: 0
  }
  startTransform = 200
  currentGame = {
    game: {}
  }
  paginationContainer
  pagination

  constructor() {}

  _getPages() {
    let gamePage = []
    let count = 15
    let pageNumber = 0

    for (let i = 0; i < this.data.gamesList.length; i++) {
      if (i < count) {
        gamePage.push(this.data.gamesList[i])
      } else {
        this.data.pages[pageNumber] = {id: pageNumber+1,games:gamePage.slice(0)}
        gamePage = []
        gamePage.push(this.data.gamesList[i])
        count += 15
        pageNumber++
      }
    }
  }

  _addPagination() {
    let div = document.createElement('div');
    div.innerHTML = paginationTemplate(this.data);
    document.querySelector('footer').insertAdjacentHTML('beforebegin', div.innerHTML);
    this._paginationListener();
  }

  _animationPagination() {
    // eslint-disable-next-line max-len
    this.paginationContainer.style.transform = 'translateX(' + (this.startTransform - this.data.currentPage * 100) + 'px)'
  }

  _paginationListener() {
    this.paginationContainer = getElement('pagination--number');
    this.pagination = getElement('pagination');
    this.pagination.addEventListener('click', (event) => {
      this.changePage(event.target);

      router(event.target, this.data.currentPage)
    })
  }

  async createGameCard(id) {
    let request = await getRequest(id)
    await this._getOneGame(request.requestUrl, request.options)
    let div = document.createElement('div')
    div.classList.add('game--card')
    div.id = this.currentGame.id
    this.currentGame.description = CutHTMLTagsFromText(this.currentGame.description)
    div.innerHTML = cardTemplate(this.currentGame)
    document.body.append(div)
    console.log(this.currentGame);
    this._addListenerGameCard()
  }

  _addListenerGameCard() {
    document.querySelectorAll('.game--card__screenshots--item').forEach(el => {
      el.addEventListener('click',() => {
        if (el.style.transform) {
          el.style.transform = ''
        } else {
          el.style.transform = `${TranslateToCenter(el)} scale(3)`
        }
      })
    })
    document.querySelector('.game--card__backToList').addEventListener('click', () => {
      history.back()
      saveToLocalStorage(document.location.pathname)
      document.querySelector('.game--card').remove()
    })
  }

  async _addListenerGameListItem() {
    const el = document.querySelectorAll('.game--list__item')

    await el.forEach(el => {
       el.addEventListener('click', () => {
          router(el)
          this.createGameCard(el.id)
      })
    })
  }

  changePage(el) {
    const paginationPage = document.querySelectorAll('.pagination--page')

    paginationPage.forEach(el => {
      el.classList.remove('active')
    })

    if (el.classList.contains('--next') && this.data.currentPage < this.data.pages.length - 1) {
      window.scrollTo(0,0)
      this.data.currentPage++
      this._animationPagination()
      this._pagesRender()
    } else if (el.classList.contains('--prev') && this.data.currentPage > 0) {
      this.data.currentPage--
      window.scrollTo(0,0)
      this._animationPagination()
      this._pagesRender()
    } else if (el.classList.contains('pagination--page')) {
      this.data.currentPage = el.innerText - 1
      window.scrollTo(0,0)
      this._animationPagination()
      this._pagesRender()
    } else {
      window.scrollTo(0,0)
      this._pagesRender()
    }

    paginationPage.forEach(el => {
      if (Number(el.innerHTML) === this.data.currentPage + 1) {
        window.scrollTo(0,0)
        el.classList.add('active')
      }
    })
    // this._openGameCard()
  }

  _pagesRender() {
    let main = document.querySelector('main')
    main.innerHTML = gameListTemplate(this.data.pages[this.data.currentPage])
    this._addListenerGameListItem()
  }

  async _getOneGame(url, options) {
    await fetch(url, options)
    .then(response => response.json())
    .then(json => this.currentGame = json)
  }

  async _getGameList() {
    let request = getRequest()
    await fetch(request.requestUrl,request.options)
    .then(response => response.json())
    .then(json => json.forEach(i => this.data.gamesList.push(i)))
  }

 async init() {
    await this._getGameList().then(() => {this._getPages()})
    await this._pagesRender()
    await this._addPagination()
  }
}