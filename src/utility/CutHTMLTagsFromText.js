export function CutHTMLTagsFromText (text) {
    const regExp = /( |<([^>]+)>)/ig ;
    return text.replace(regExp, ' ');
}