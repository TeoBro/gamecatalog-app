export function getRequest( id = null , tag = [], platform = null, sortBy = null) {
// Вытащить переменные из .env
// let variable  = process.env
//   const {ACCESS_VAR, URL_API} = variable

  let request = {
    requestUrl: 'https://mmo-games.p.rapidapi.com/',
    options: {
      method: 'GET',
      headers: {
          'X-RapidAPI-Key': '0d21f826aamsh710e81946ac30bcp1628efjsnb515b1a9e37a',
        'X-RapidAPI-Host': 'mmo-games.p.rapidapi.com'
      }
    }
  }

  platform = checkPlatform(platform)
  let type = checkType(id, tag, platform, sortBy)

  switch(type) {
    case 'GamesList':
      request.requestUrl += 'games'
      return request
    case 'OneGame':
      request.requestUrl += `game?id=${id}`
      return request
    case 'Sort':
      request.requestUrl+= `games?sort-by=${sortBy}`
      return request
    case 'Platform':
      request.requestUrl += `games?platform=${platform}`
      return request
    case 'Category':
      request.requestUrl += `games?category=${tag[0].split(' ').join('')}`
      return request
    case 'MultiRequest':
      request.requestUrl += `games?platform=${platform}&category=${tag[0]}&sort-by=${sortBy}`
      return request
    case 'MultiTag':
      request.requestUrl += `filter?tag=${tag.join('.')}&platform=${platform ? platform : 'all'}`
      return request
  }
}

function checkPlatform(platform) {

  if(platform === 'Web Browser') {
    platform = 'browser'
  } else if(platform === 'PC (Windows)') {
    platform = 'pc'
  }
  return platform
}


function checkType(id, tag, platform, sortBy ) {

  if (id === null && tag?.length > 1) {
    return 'MultiTag'
  } else if (id === null && tag.length === 1 && platform && sortBy) {
    return 'MultiRequest'
  } else if (id === null && tag.length === 0 && platform === null && sortBy) {
    return 'Sort'
  } else if (id === null && tag.length === 0 && platform && sortBy === null) {
    return 'Platform'
  } else if (id === null && tag.length === 1 && platform === null && sortBy === null) {
    return 'Category'
  } else if (id && tag?.length === 0 && platform === null && sortBy === null) {
    return 'OneGame'
  } else {
    return 'GamesList'
  }
}
