export function TranslateToCenter(el) {
  const windowInnerWidth = window.innerWidth
  const windowInnerHeight = window.innerHeight
  const elCoordinate = el.getBoundingClientRect()
  const elY = elCoordinate.y
  const elX = elCoordinate.x
  const elHeight = el.offsetHeight
  const elWidth = el.offsetWidth
  console.log(elCoordinate);
  return `translate(${(windowInnerWidth / 2) - elX - elWidth / 2}px, ${(windowInnerHeight / 2) - elY - elHeight / 2 }px)`
}