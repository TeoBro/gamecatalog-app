export function RemovePagination() {
    let pagination = document.querySelector('.pagination')
    if(pagination) {
      pagination.remove()
    } else {
      console.log('pagination not found');
  }
}