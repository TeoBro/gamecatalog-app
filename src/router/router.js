import routerConfig from './routerConfig';
import {HomePage} from '@/views/HomePage';
import {RemovePagination} from '@/utility/RemovePagintaion';
import {GameListApp} from '@/views/GamesListApp';
import {saveToLocalStorage} from "@/utility/SaveToLocalStorage";



export function router (event, currentPage = 0, storagePage) {
  localStorage.clear()
  if (storagePage) {
    _navigate(null, null, storagePage)
    history.pushState({page: storagePage}, '', storagePage)

  } else {
    _navigate(event, currentPage);
  }

  window.onpopstate = _popState;
}

function _navigate(e, currentPage, storagePage) {
  let page;
  if (storagePage) {
    _checkPageType(storagePage)

  } else {
    if (e.classList.contains('pagination--btn') || e.classList.contains('pagination--page')) {
      page = document.location.pathname + `?page=${currentPage}`
      history.pushState({page: page}, '', page)

    } else if (e.hasAttribute('href')) {
      page = e.getAttribute('href')

      if (page === 'gamecatalog') {
        page += `?page=${currentPage}`
      }


      history.pushState({page: page}, '', page);
      _checkPageType(page);
    } else if (e.classList.contains('game--list__item')) {
      page = document.location + `&id=${e.id}`;
      _checkPageType(page);
      history.pushState({page: page}, '', page);
    }
  }


  saveToLocalStorage(page)
}

function _popState(e) {

  if (document.querySelector('.game--card')) {
    document.querySelector('.game--card').remove();
  }

  let page = (e.state && e.state.page) || routerConfig.mainPages;
  _checkPageType(page);
}


export function _checkPageType(page) {
  let game = new GameListApp()
  let searchParams = new URLSearchParams(document.location.search);
  let id = searchParams.get('id')
  let pageNumber = searchParams.get('page')

  if (searchParams.has('id') && searchParams.has('page')) {
    RemovePagination();
    game.data.currentPage = pageNumber
    game.init().then(() => {
      game.createGameCard(id)
    })

  } else if (searchParams.has('page')) {
    RemovePagination()
    game.data.currentPage = pageNumber
    game.init()

  } else {
    _loadPage(page)
  }
}

function _loadPage(page) {
  let data = page;
  let currentPage;

  if (page.match('gamecatalog')) {
    data = new URLSearchParams(page)
    currentPage = data.get('page')
    page = 'gamecatalog'
  }

  switch(page) {

    case 'home':
      new HomePage().init();
      RemovePagination();
      break;

    case 'gamecatalog': {
      RemovePagination();
      let game = new GameListApp()
      game.data.currentPage = currentPage || 0
      game.init()
      break;
    }

    case 'contacts':
      let template = require(`../partial/contacts.handlebars`);
      document.querySelector('main').innerHTML = template({});
      RemovePagination();
      break;

    default:
      RemovePagination();
      new HomePage().init();
      break;
  }
}
