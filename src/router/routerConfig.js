module.exports = {
  mainPages: 'home',
  pages: {
    Home: {
      title: 'Главная',
      menu: 'home'
    },
    GameCatalog: {
      title: 'Каталог Игр',
      menu: 'games'
    },
    Films: {
      title: 'Каталог Фильмов',
      menu: 'films'
    },
    Music: {
      title: 'Каталог Музыки',
      menu: 'music'
    },
    Contacts: {
      title: 'Контакты',
      menu: 'contacts'
    }
  }
}