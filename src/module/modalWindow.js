export function modalWindowListener() {
  const sort = document.querySelector('.sort')
  const btnContact = document.querySelector('.header--btn')
  const btnModal = document.querySelector('.modal--btn')
  const modalWrapper = document.querySelector('.modal')
  const body = document.querySelector('body')

  btnContact.addEventListener('click', (event) => {
    event.stopPropagation()
    event.preventDefault()
    // sort.classList.add('--hide')
    console.log(modalWrapper.classList);
    if (modalWrapper.classList.contains('--open')) {

      btnModalClose()
    } else {
      modalWrapper.classList.add('--open')
      body.classList.add('stop-scrolling')

      window.addEventListener('scroll', () => {
        modalWrapper.style.top =`${window.pageYOffset + 75}px`
      })
    }
  })

  btnModal.addEventListener('click', (event) => {
    console.log('modal btn')
    modalWrapper.classList.remove('--open')
    modalWrapper.classList.add('--close')
    btnModalClose()
  })

  function btnModalClose() {
    console.log('close');
    modalWrapper.classList.remove('--open')
    modalWrapper.classList.add('--close')

    setTimeout(() => {
      modalWrapper.classList.remove('--close')
      body.classList.remove('stop-scrolling')
    }, 550)
  }

}
