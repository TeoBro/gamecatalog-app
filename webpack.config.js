const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CopyPlugin = require('copy-webpack-plugin')
const HandlebarsPlugin = require("handlebars-webpack-plugin");
const {DefinePlugin} = require("webpack");
const dotenv = require('dotenv').config( {
  path: path.join(__dirname, '.env')
} );

const isProd = process.env.NODE_ENV === 'production'
const isDev = !isProd

const jsLoaders = () => {
  const loaders = [
    {
      loader: 'babel-loader',
      options: {
        presets: ['@babel/preset-env'],
      }
    }
  ]
  isDev && loaders.push('eslint-loader')
}

module.exports = {
  context: path.resolve(__dirname, 'src'),
  mode: 'development',
  entry: ['@babel/polyfill', './index.js'],
  output: {
    filename: 'bundle.[hash].js',
    path: path.resolve(__dirname, 'dist'),
    clean: true
  },
  resolve: {
    extensions: ['.js'],
    alias: {
      '@': path.resolve(__dirname, 'src')
    },
    fallback: {
      "fs": false,
      "path": false,
      "os": false
    },
  },
  devServer: {
    port: 4000,
    historyApiFallback: true,
    hot: isDev
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].[hash].css',
    }),
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(__dirname, 'src/assets/logo.png'),
          to: path.resolve(__dirname, 'dist')
        },
        {
          from: path.resolve(__dirname, 'src/assets/icons/social-email.svg'),
          to: path.resolve(__dirname, 'dist')
        },
        {
          from: path.resolve(__dirname, 'src/assets/icons/social-phone.svg'),
          to: path.resolve(__dirname, 'dist')
        },
        {
          from: path.resolve(__dirname, 'src/assets/icons/social-telegram.svg'),
          to: path.resolve(__dirname, 'dist')
        },
        {
          from: path.resolve(__dirname, 'src/assets/icons/social-vk.svg'),
          to: path.resolve(__dirname, 'dist')
        }
      ],
    }),
    new DefinePlugin( {
      "process.env": JSON.stringify(dotenv.parsed)
    }),
    // new HandlebarsPlugin({
    //   entry: path.join(__dirname, "src", "partial", "*"),
    //   partials: [
    //     path.join(__dirname,"src", "partial", "**", "*.handlebars" ),
    //   ]
    // }),
    new HtmlWebpackPlugin({
      template: 'index.html',
      minify: {
        removeComments: isProd,
        collapseWhitespace: isProd
      }
    })
  ],
  module: {
    rules: [
      {
        test: /\.handlebars$/,
        loader: 'handlebars-loader',
      },
      {
        test: /\.hbs$/,
        loader: 'handlebars-loader',
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader'
        ],
      },
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: jsLoaders()
      }
    ]
  },
}

